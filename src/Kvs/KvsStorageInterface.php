<?php

namespace NORA\Storage\Kvs;

interface KvsStorageInterface
{
    /**
     * データを保存
     */
    public function store(string $key, string $value): bool;
    public function get(string $key): string;
    public function has(string $key): bool;
}
