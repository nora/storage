<?php

namespace NORA\Storage\Kvs;

use NORA\Storage\Filesystem\FilesystemStorage;
use NORA\Storage\Filesystem\FilesystemStorageOption;
use Psr\Log\LoggerInterface;
use NORA\Logger\LoggerClientTrait;

final class KvsFilesystemStorage implements KvsStorageInterface
{
    use LoggerClientTrait;

    private FilesystemStorage $filesystem;

    public function __construct(
        FilesystemStorageOption $option,
        LoggerInterface $logger = null
    ) {
        if ($logger instanceof LoggerInterface) {
            $this->setLogger($logger);
        }
        $this->filesystem = new FilesystemStorage($option);
    }

    public function store(string $key, string $value): bool
    {
        $this->logDebug("store {$key}");
        return $this->filesystem->write($key, $value);
    }

    public function get(string $key): string
    {
        $this->logDebug("store {$key}");
        return $this->filesystem->read($key);
    }

    public function has(string $key): bool
    {
        return $this->filesystem->isFileExists($key);
    }
}
