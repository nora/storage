<?php

namespace NORA\Storage\Kvs;

use NORA\Storage\Filesystem\FilesystemStorageOption;

final class KvsStorageOption
{
    public function __construct(
        private KvsDriver $driver,
        private ?FilesystemStorageOption $filesystem = null
    ) {
    }

    public function getDriver(): KvsDriver
    {
        return $this->driver;
    }

    public function getFilesystemOption(): FilesystemStorageOption
    {
        if ($this->filesystem instanceof FilesystemStorageOption) {
            return $this->filesystem;
        }
        throw new \RuntimeException("FilesystemStorageOption is invalid");
    }
}
