<?php

namespace NORA\Storage\Kvs;

enum KvsDriver: string
{
    case Redis = 'redis';
    case Filesystem = 'filesystem';
}
