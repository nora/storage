<?php

namespace NORA\Storage;

use NORA\Storage\Filesystem\FilesystemStorageOption;
use NORA\Storage\Filesystem\FilesystemStorageInterface;

interface StorageFactoryInterface
{
    public function filesystem(FilesystemStorageOption $options): FilesystemStorageInterface;
}
