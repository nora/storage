<?php

namespace NORA\Storage;

interface StorageInterface
{
    /**
     * データを保存
     */
    public function store(string $key, mixed $value): bool;
    public function get(string $key): mixed;
}
