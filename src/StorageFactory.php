<?php

namespace NORA\Storage;

use NORA\Storage\Filesystem\FilesystemStorageOption;
use NORA\Storage\Filesystem\FilesystemStorage;
use NORA\Storage\Filesystem\FilesystemStorageInterface;
use NORA\Storage\Kvs\KvsDriver;
use NORA\Storage\Kvs\KvsStorageInterface;
use NORA\Storage\Kvs\KvsStorageOption;
use NORA\Storage\Kvs\KvsFilesystemStorage;
use Psr\Log\LoggerInterface;
use Ray\Di\Di\Assisted;

class StorageFactory implements StorageFactoryInterface
{
    public function filesystem(FilesystemStorageOption $options, #[Assisted] ?LoggerInterface $logger = null): FilesystemStorageInterface
    {
        return new FilesystemStorage($options, $logger);
    }

    public function kvs(
        KvsStorageOption $options,
        #[Assisted] ?LoggerInterface $logger = null
    ): KvsStorageInterface {
        return match ($options->getDriver()) {
            KvsDriver::Filesystem => new KvsFilesystemStorage(option: $options->getFilesystemOption(), logger: $logger),
            KvsDriver::Redis => new KvsFilesystemStorage(option: $options->getFilesystemOption(), logger: $logger)
        };
    }
}
