<?php

namespace NORA\Storage\Filesystem;

use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

class FilesystemStorage implements FilesystemStorageInterface
{
    public function __construct(
        private FilesystemStorageOption $option,
        private ?LoggerInterface $logger = null
    ) {
        // 初期化
        $path = $option->getPath();
        if (!is_dir($path)) {
            mkdir($path, 0770, true);
        }
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    private function getPath(string $path): string
    {
        return implode(
            "/",
            [
                rtrim($this->option->getPath(), "/"),
                trim($path, "/")
            ]
        );
    }

    public function write(string $path, string $contents): bool
    {
        $file = $this->getPath($path);
        if (file_put_contents($file, $contents)) {
            return true;
        }
        return false;
    }

    /**
     * @param resource|mixed $stream
     */
    public function writeStream(string $path, $stream): bool
    {
        $file = $this->getPath($path);
        if (!is_resource($stream)) {
            return false;
        }
        if (file_put_contents($file, stream_get_contents($stream))) {
            return true;
        }
        return false;
    }

    public function isFileExists(string $path): bool
    {
        $file = $this->getPath($path);
        return file_exists($file);
    }

    public function read(string $path): string
    {
        $file = $this->getPath($path);
        if (is_readable($file)) {
            $contents = file_get_contents($file);
            if (is_string($contents)) {
                return $contents;
            }
            throw new \RuntimeException("maybe broken {$file}");

        }
        throw new \RuntimeException("can not read {$file}");
    }

    /**
     * @return resource
     */
    public function readStream(string $path)
    {
        $file = $this->getPath($path);
        $fp = fopen($file, "r");
        if (is_resource($fp)) {
            return $fp;
        }
        throw new \RuntimeException("can not open {$file}");
    }

    private function info(string $message): void
    {
        if ($this->logger) {
            $this->logger->info($message);
        }
    }

    public function removeDirectory(string $path): void
    {
        $root_path = $this->getPath($path);

        if (!is_dir($root_path)) {
            throw new \RuntimeException("{$root_path} is not a directory");
        }

        $this->info("{$root_path} deleting");

        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($root_path, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                $this->info("directory {$file->getPathname()} is deleting");
                rmdir($file->getPathname());
            } else {
                $this->info("file {$file->getPathname()} is deleting");
                unlink($file->getPathname());
            }
        }
        unset($iterator);
        rmdir($root_path);
    }
}
