<?php

namespace NORA\Storage\Filesystem;

use Psr\Log\LoggerInterface;

interface FilesystemStorageInterface
{
    public function write(string $path, string $contents): bool;
    /**
     * @param resource $stream
     */
    public function writeStream(string $path, $stream): bool;
    public function read(string $path): string;
    public function isFileExists(string $path): bool;
    /**
     * @return resource|false
     */
    public function readStream(string $path);
    public function removeDirectory(string $path): void;
    public function setLogger(LoggerInterface $logger): void;
}
