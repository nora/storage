<?php

namespace NORA\Storage\Filesystem;

final class FilesystemStorageOption
{
    public function __construct(
        private string $path
    ) {
    }

    public function getPath(): string
    {
        return $this->path;
    }
}
