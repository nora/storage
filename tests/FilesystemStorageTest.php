<?php

declare(strict_types=1);

namespace NORA\Storage;

use NORA\Storage\Filesystem\FilesystemStorageOption;
use NORA\Storage\Filesystem\FilesystemStorage;
use PHPUnit\Framework\TestCase;
use stdClass;
use NORA\Logger\MakeLogger;
use NORA\Storage\Filesystem\FilesystemStorageInterface;

class FilesystemStorageTest extends TestCase
{
    protected StorageFactory $storageFactory;

    protected function setUp(): void
    {
        $this->storageFactory = new StorageFactory();
    }

    /**
     * @test
     */
    public function testCreateFilesystem(): FilesystemStorageInterface
    {
        $storage = $this->storageFactory->filesystem(new FilesystemStorageOption(path: __DIR__ . '/var/storage'));
        $this->assertInstanceOf(FilesystemStorage::class, $storage);
        return $storage;
    }

    /**
     * @test
     * @depends testCreateFilesystem
     */
    public function testFileReadWrite(FilesystemStorage $storage): void
    {
        $storage->write('test', 'hoge');
        $this->assertFileExists(__DIR__ . '/var/storage/test');
        $this->assertEquals('hoge', $storage->read('test'));
        // Write Object
        $object = new stdClass();
        $object->test = "hoge";
        $storage->write('test', serialize($object));
        $result = unserialize($storage->read('test'));
        assert(is_object($result));
        assert(property_exists($result,"test"));
        $this->assertEquals('hoge', $result->test);
    }

    /**
     * @test
     * @depends testCreateFilesystem
     */
    public function testRemoveFiles(FilesystemStorage $storage): void
    {
        $storage->setLogger((new MakeLogger(path: __DIR__ . '/var', name: "logger.log"))());
        $storage->removeDirectory("/");
        $this->assertTrue(!file_exists(__DIR__ . '/var/storage/test'));
    }
}
