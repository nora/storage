<?php

declare(strict_types=1);

namespace NORA\Storage;

use NORA\Storage\Filesystem\FilesystemStorageOption;
use NORA\Storage\Kvs\KvsDriver;
use NORA\Storage\Kvs\KvsStorageOption;
use PHPUnit\Framework\TestCase;

class KvsStorageTest extends TestCase
{
    protected StorageFactory $storageFactory;

    protected function setUp(): void
    {
        $this->storageFactory = new StorageFactory();
    }

    /**
     * @test
     */
    public function testReadWrite() : void
    {
        $storage = $this->storageFactory->kvs(
            new KvsStorageOption(
                driver: KvsDriver::Filesystem,
                filesystem: new FilesystemStorageOption(path: __DIR__ . '/var/storage/kvs')
            )
        );

        $this->assertTrue($storage->store('test', 'abc'));
    }
}
